def rec_fibbonaci_series(number):
    #menyra rekursive nqs jane dy termat e pare kthehet perkatesisht vlera e tyre ,perdryshe therritet rekursivisht me parametra
    #  n-2 dhe n-1 qe te dal seria .
    if number==0:
        return 0
    elif number==1:
        return 1
    else:
        return rec_fibbonaci_series(number-2)+rec_fibbonaci_series(number-1)


number=input("Enter a number")
for x in range(int(number)):
    print (rec_fibbonaci_series(x))