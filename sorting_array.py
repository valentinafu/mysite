def sort_array(element_list):
    tmp=0
    #selection sort ,krahasojme ndermjet dy cikleve cdo element ,ne baze te kushtit
    #  vendosim ne temp vleren qe sploteson  kushtin dhe me pas ndryshojme vendet e dy elementeve
    #vendit te elementit tjeter i japim vleren e temp
    for element in range(len(element_list)-1):
        for next_element in range(element,len(element_list)):
            if element_list[element]>element_list[next_element]:
                tmp=element_list[element]
                element_list[element]=element_list[next_element]
                element_list[next_element]=tmp
    return element_list


my_list=[2, 3, 10 , 1 , 7]
my_sorted_list=sort_array(my_list)
for el in my_sorted_list:
    print(el)