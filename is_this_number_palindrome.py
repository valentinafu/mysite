def is_number_palindrome(number):
    found=0
    #e ruajme numrin ne format string dhe kontrollojme duke kapur elementin e pare
    #  me te fundit nqs jane ye ndryshem qe aty e kuptojme qe sjane palindrome ndaj dalim nga cikli me variablin found
    for x in range(len(number)//2):
        if number[x]!=number[len(number)-1]:
            found=1
    if(found==1):
        return False
    else:
        return True

number=input("Enter a number")
if is_number_palindrome(number):
    print("number is palindrome")
else:
    print("number is not palindrome")