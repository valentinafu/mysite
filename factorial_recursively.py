def factorial_recursive(number):
    #menyra rekursive ,nqs eshte 1 ose 0 pasi 0! dhe 1! jane 1 do ktheje 1 perndryshe do marr vet numrin dhe do therrase funksionin duke kaluar si
    # parameter numrin e zvogeluar me 1 derisa plotesohet kushti i pare
    if number==1 or number==0:
      return 1
    else :
        return number*factorial_recursive(number-1)


number=input("Enter a number")

print("Factorial is " + str(factorial_recursive(int(number))))